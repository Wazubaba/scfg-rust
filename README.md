Simple ConFiGuration file library
=================================
This is a basic library for simple configuration files.
A configuration file consists of String keys and values.

See the included rustdoc api(should be pretty concise
and approachable. If not, lemme know!) for usage.


Usage
=================================
Edit your Cargo.toml file, add this:
```
[dependencies.scfg]
git = "git://wazu.info.tm/scfg-rust.git"
```

Alternatively, if my server is acting up or you prefer
to stick to github:
```
[dependencies.scfg]
git = "https://github.com/Wazubaba/scfg-rust.git"
```

Now you can simply add `extern crate scfg;` to your
code and it should be downloaded by cargo on your
next build. Don't forget to freshen it from
time-to-time with `cargo update`!


Dependencies
=================================
This depends on my wio library(should automagically get
downloaded with cargo), and was tested with:
`rustc 1.0.0-beta.2 (e9080ec39 2015-04-16) (built 2015-04-16)`

Probably ought to work with more recent versions provided
nothing in the main stdlib changes too much.


License
=================================
I didn't re-invent the wheel or anything here. MIT is
fine for this. If you need another license, lemme
know and we probably can work something out.

