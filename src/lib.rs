use std::io::prelude::*;
use std::path::Path;
use std::collections::HashMap;

extern crate wio;


/// The main container class for a configuration file.
pub struct Scfg<'a> {
    /// The path to the file.
	fname: &'a Path,
	//data: HashMap<&'a str,&'a str>,
	/// The HashMap containing our file data.
	data: HashMap<String, String>,
	/// What line of the configuration file an error occurred on.
	line_err: i32,
	/// Whether we successfully parsed the file or not.
	parsed:   bool,
}

/*
 * For the error reporting, when validate returns false,
 * we dump the error into self.error, which can be
 * formatted up niceley via self.report().
 *
 * self.report() should include number of lines, any errors
 * with the line they occurred on clearly defined, and
 * whether parsing failed or succeeded.
*/

impl<'a> Scfg<'a>
{
	/// Constructs a new Scfg entity, returning it.
	///
	/// # Arguments
	/// * `path`  - A valid `Path` pointing to an scfg file.
	///
	/// # Example
	/// ```
	/// use std::path::Path;
	/// use scfg::Scfg;
	///
	/// fn main() {
	/// 	let test_file = Path::new("test.scfg");
	/// 	let mut my_config = Scfg::new(test_file);
	/// }
	/// ```
    pub fn new(path: &'a Path) -> Scfg
    {
        Scfg {
            fname: path,
            data:  HashMap::new(),
            line_err: 0,
            parsed: false,
        }
    }

	/// Parses an scfg file to validate it's syntax, optionally loading it.
	/// Returns true if successful or false (and sets `.line_err` and `.parsed`
	/// appropriately.).
	///
	/// # Arguments
	/// * `data`     - A `&str` containing the data from the config.
	/// * `testing`  - A `bool` that tells .validate() whether it should load
	/// the data into the .data `HashMap`. `true` tells it to validate only.
	///
	/// # Examples
	/// ```
	/// use std::path::Path;
	/// use scfg::Scfg;
	///
	/// fn main() {
	/// 	let test_file = Path::new("test.scfg");
	/// 	let mut my_config = Scfg::new(test_file);
	///
	/// 	if my_config.validate("mykey = myval\ncat = nya\ndog = woof\n", true) {
	/// 		println!("Validated successfully!");
	/// 	} else {
	/// 		println!("Could not validate data. :(");
	/// 	}
	/// }
	/// ```
	pub fn validate(&mut self, data: &str, testing: bool) -> bool
	{
		let mut line_num: i32 = 0;

		// Did this really fix it? O_O
		// Oh my god... it did! O_O
		let mut key: String;// = String::new();
		let mut val: String;// = String::new();

		let lines = data.split("\n");
		for item in lines {
			line_num += 1;
			if item == "" { continue }; // I have no clue why this is needed...

			// Test if this is even a valid line
			if item.contains("=") == false {
				self.line_err = line_num;
				self.parsed   = false;
				return false
			}

			let mut token = item.split("=");
			if token.clone().count() != 2 {
			    self.line_err = line_num;
			    self.parsed   = false;
				return false
			}

			let work = token.next().unwrap();
			key = work.trim_matches(' ').to_string();

			let work = token.next().unwrap();
			val = work.splitn(2, ' ').nth(1).unwrap().to_string();

			if !testing {
				self.data.insert(key, val);
			}
		}
        self.parsed = true;
		return true
	}

	/// Displays a list of {key} = {value} to stdout or wherever print goes :P
	///
	/// # Example
	/// ```
	/// use std::path::Path;
	/// use scfg::Scfg;
	///
	/// fn main() {
	/// 	let test_file = Path::new("test.scfg");
	/// 	let mut my_config = Scfg::new(test_file);
	///
	/// 	if my_config.load() {
	/// 		my_config.display();
	/// 	} else {
	/// 		panic!("Could not load config!");
	/// 	}
	/// }
	/// ```
	pub fn display(self)
	{
		for key in self.data.keys() {
			println!("{} = {}", key, self.data.get(key).unwrap());
		}
	}

	/// Actually load the configuration file, parsing it into the .data map.
	///
	/// # Example
	/// ```
	/// use std::path::Path;
	/// use scfg::Scfg;
	///
	/// fn main() {
	/// 	let test_file = Path::new("test.scfg");
	/// 	let mut my_config = Scfg::new(test_file);
	/// 	my_config.load();
	///
	/// 	my_config.display();
	/// }
	/// ```
	pub fn load(&mut self) -> bool
	{
		let mut file = match wio::fopenp(self.fname, "r") {
			Ok(file) => file,
			Err(_) => return false,//panic!("Cannot load file! err:{}", err),
		};

		let mut fdat: String = String::new();
		let _ = file.read_to_string(&mut fdat);

		if self.validate(&fdat, false) {
			return true
		} else {
			println!("Awww :( T_T");
			return false
		}
	}
}

#[test]
fn new_test() {
	let mut rawr = Scfg::new(Path::new("test.scfg"));
	if rawr.load() {
		println!("Loaded config successfully \\o/");
	} else {
		println!("AUGH");
	}
	rawr.display();
}
